<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::get('/proses_tambah', 'CrudController@tambahdata');
Route::get('/lihatkegiatan', 'CrudController@lihatdata');

Auth::routes();

Route::get('/home', function(){
	return view('home');
});
Route::get('/gagal', function(){
	return view('gagal');
});
Route::get('/tambahkegiatan', function(){
	return view('tambah_kegiatan');
});

Route::post('tambahlogin', 'CrudController@tambahlogin');
Route::post('dashboard', 'CrudController@login');
