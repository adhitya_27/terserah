<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
 use Input; 
 use Illuminate\Support\Facades\DB; 
 use Redirect;
use Auth;
use  App\User;
use  App\siswa;

use  Illuminate\Support\Facades\Hash ;
use Illuminate\Support\Facades\Session ;
use View;
class CrudController extends Controller
{
    //proses_tambah
    public function tambahdata(Request $request){
    		$user = User::all();
    		$data=array(
    		'id_kegiatan' => Input::get('id_kegiatan'),
    		'kegiatan' 	  => Input::get('kegiatan'),
    		'hasil' 	  => Input::get('hasil'),
    		'tindaklanjut' => Input::get('tindaklanjut'),
    		'tanggal' => Input::get('tanggal'),
    		'id_users' => Input::get('id_users'),


    	);
    	DB::table('activity')->insert($data);
    	return Redirect::to('/lihatkegiatan')->with('message', 'berhasil menambahkan data');


    }
    //index
    public function lihatdata(){
    	$data = DB::table('activity')->get();

    	return View::make('lihatkegiatan')->with('activity', $data);
    }
    //proses_tambah
    public function tambahlogin(Request $request){
            $data =  new User();
        $data->name = $request->name;
        $data->password = bcrypt($request->password);
        $data->save();
        return redirect('login')->with('alert-success','Kamu berhasil Register');
    
    }
    public function login(Request $request){

  //   	$users = DB::select('select * from login');
		// return view('home',['users'=>$users]);
  //   	if (Auth::attempt(['email'=>Input::post('email'), 'password'=> Input::post('password')])) {
  //   		if (Auth::user()->hak_akses=="admin") {
  //   			echo "Admin";
  //   		} else{
  //   			echo "USERS!!!";
  //   		}
  //   return view('home');
  //   	} else{
  //   			echo "<div id='card-alert' class='card red'>";
  //   			echo "<div class='card-content white-text'>";
  //   			echo "<p> FAILED LOGIN!!!</p>";
  //   			echo "</div>";
  //   			echo "<button type='button' class='close white-text' data-dismiss='alert' aria-label='Close'>";
  //   			echo " <span aria-hidden='true'>×</span>
  //                     </button>
  //                   </div>";
  //   return view('gagal');
  //   	}
        $id_users = $request->id_users;
    	$email  =  $request->email ;
        $password  =  $request->password ;
        $data  =  User::where('email', $email)->first();
        if ( count($data)>0 ) { // whether the email exists or not
            if ( Hash::check($password , $data->password )) {
                Session::put('name', $data->name );
                Session::put('id_users', $data->id_users );
                Session::put('email', $data->email );
                Session::put('login', TRUE );
                return view('home');
            }
            else {
                return view('gagal');
            }
        }
        else {
            return redirect('gagal');
        }
    }
}
