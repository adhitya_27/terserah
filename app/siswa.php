<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use ModelUser;
class siswa extends Model
{
    //
    	# Tentukan nama tabel terkait
	protected $table = 'activity';
	protected $primaryKey = 'id_kegiatan';
	# MASS ASSIGNMENT
	# Untuk membatasi attribut yang boleh di isi (Untuk keamanan)
	protected $fillable = array('id_kegiatan','kegiatan', 'hasil', 'tindaklanjut', 'tanggal');

	/*
	 * Relasi One-to-One
	 * =================
	 * Sebaliknya, buat function bernama mahasiswa(), dimana model 'Wali' memiliki relasi One-to-One (belongsTo)
	 * sebagai timbal balik terhadap model 'Mahasiswa'
	 */
}
