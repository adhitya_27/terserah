<!DOCTYPE html>
<html>
<head>
    <title>hello</title><!-- 
<link rel="stylesheet" type="text/css" href="/css/materialize.css"> -->
<link rel="stylesheet" type="text/css" href="/css/materialize/css/materialize_OLD.css">
<link rel="stylesheet" type="text/css" href="/css/style.css">
  <style type="text/css">
  .input-field div.error{
    position: relative;
    top: -1rem;
    left: 0rem;
    font-size: 0.8rem;
    color:#FF4081;
    -webkit-transform: translateY(0%);
    -ms-transform: translateY(0%);
    -o-transform: translateY(0%);
    transform: translateY(0%);
  }
  .input-field label.active{
      width:100%;
  }
  .left-alert input[type=text] + label:after, 
  .left-alert input[type=password] + label:after, 
  .left-alert input[type=email] + label:after, 
  .left-alert input[type=url] + label:after, 
  .left-alert input[type=time] + label:after,
  .left-alert input[type=date] + label:after, 
  .left-alert input[type=datetime-local] + label:after, 
  .left-alert input[type=tel] + label:after, 
  .left-alert input[type=number] + label:after, 
  .left-alert input[type=search] + label:after, 
  .left-alert textarea.materialize-textarea + label:after{
      left:0px;
  }
  .right-alert input[type=text] + label:after, 
  .right-alert input[type=password] + label:after, 
  .right-alert input[type=email] + label:after, 
  .right-alert input[type=url] + label:after, 
  .right-alert input[type=time] + label:after,
  .right-alert input[type=date] + label:after, 
  .right-alert input[type=datetime-local] + label:after, 
  .right-alert input[type=tel] + label:after, 
  .right-alert input[type=number] + label:after, 
  .right-alert input[type=search] + label:after, 
  .right-alert textarea.materialize-textarea + label:after{
      right:70px;
  }
  body {
    background-image: url('/img/bg_login.jpg');
    background-size: cover;
  }
  </style>
</head>
<body>

          <!--jqueryvalidation-->
          <div id="jqueryvalidation" class="section">
            <div class="row">
              <div class="col s12">
                  <div class="col s12 m4 offset-m8">
                        <div class="card-panel">
                            <h4 class="center" style="margin-top: 148px;">Login Dulu!!!</h4>

@if(Session::has('message'))
<span>{{Session::get('message')}}</span>
@endif
                            <div class="row">
                                {!! Form::open(['url'=>'dashboard', 'class'=>'formValidate', 'method'=>'post', 'id'=>'formValidate']) !!}
                                    <div class="row">
                                        <div class="input-field col s12">
                                            {!! Form::label('Email', null, ['for'=>'cemail']) !!}
                                            {!! Form::email('email', null, ['id'=>'cemail', 'data-error'=>'.errorTxt1']) !!}
                                            <div class="errorTxt1"></div>
                                        </div>
                                        <div class="input-field col s12">
                                            {!! Form::label('Password', null, ['for'=>'cemail']) !!}
                                            {!! Form::password('password', null, ['id'=>'cemail', 'data-error'=>'.errorTxt2']) !!}
                                            <div class="errorTxt2"></div>
                                        </div>
                                        <div class="row">
                                        <div class="input-field col s8 m4">
                                            <button class="btn waves-effect waves-light submit" type="submit" name="action">Submit
                                            </button>
                                        </div>
                                        <div class="col s4 m8"><p style="font-size: 12.5px;">Don't have account? <a href="/register">SignUp Now!</a></p></div></div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <!--angularjs-->
    <script type="text/javascript" src="js/plugins/angular.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.js"></script>
    <!--prism -->
    <script type="text/javascript" src="js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="js/plugins/chartist-js/chartist.min.js"></script>
    
    <!-- chartist -->
    <script type="text/javascript" src="js/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery-validation/additional-methods.min.js"></script>
    
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="js/custom-script.js"></script>
    <script type="text/javascript">
    $("#formValidate").validate({
        rules: {
            uname: {
                required: true,
                minlength: 5
            },
            email: {
                required: true,
                email:true
            },
            password: {
                required: true,
                minlength: 5
            },
            cpassword: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            curl: {
                required: true,
                url:true
            },
            crole:"required",
            ccomment: {
                required: true,
                minlength: 15
            },
            cgender:"required",
            cagree:"required",
        },
        //For custom messages
        messages: {
            uname:{
                required: "Enter a username",
                minlength: "Enter at least 5 characters"
            },
            curl: "Enter your website",
            email:{
              required: "Enter Your Email!",
            }
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
     });
    </script>
</body>
</html>